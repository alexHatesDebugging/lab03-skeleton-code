package course.labs.activitylab

import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView

class ActivityOne : Activity() {

    // lifecycle counts
    //TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
    //TODO:  increment the variables' values when their corresponding lifecycle methods get called and log the info
    private lateinit var createView: TextView
    private lateinit var startView: TextView
    private lateinit var resumeView: TextView
    private lateinit var pauseView: TextView
    private lateinit var stopView: TextView
    private lateinit var restartView: TextView
    private lateinit var destroyView: TextView
    internal var onCreateCounter: Double = 0.toDouble()
    internal var onStartCounter: Double = 0.toDouble()
    internal var onResumeCounter: Double = 0.toDouble()
    internal var onPauseCounter: Double = 0.toDouble()
    internal var onStopCounter: Double = 0.toDouble()
    internal var onRestartCounter: Double = 0.toDouble()
    internal var onDestroyCounter: Double = 0.toDouble()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)
        createView=findViewById(R.id.create) as TextView
        startView=findViewById(R.id.start) as TextView
        resumeView=findViewById(R.id.resume) as TextView
        pauseView=findViewById(R.id.pause) as TextView
        stopView=findViewById(R.id.stop) as TextView
        restartView=findViewById(R.id.restart) as TextView
        destroyView=findViewById(R.id.destroy) as TextView
        onCreateCounter++
        //Log cat print out
        Log.i(TAG, "onCreate called $onCreateCounter time(s)")
        createView.text="onCreate has been called:$onCreateCounter time"
        //TODO: update the appropriate count variable & update the view
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_one, menu)
        return true
    }

    // lifecycle callback overrides

    public override fun onStart() {
        super.onStart()
        onStartCounter++
        //Log cat print out
        Log.i(TAG, "onStart called $onStartCounter time(s)")
        startView.text="onStart has been called:$onStartCounter time"
        //TODO:  update the appropriate count variable & update the view
    }

    public override fun onResume() {
        super.onResume()
        onResumeCounter++
        //Log cat print out
        Log.i(TAG, "onResume called $onResumeCounter time(s)")
        resumeView.text="onResume has been called:$onResumeCounter time"
        //TODO:  update the appropriate count variable & update the view
    }
    public override fun onPause() {
        super.onPause()
        onPauseCounter++
        //Log cat print out
        Log.i(TAG, "onPause called $onPauseCounter time(s)")
        pauseView.text="onPause has been called:$onPauseCounter time"
        //TODO:  update the appropriate count variable & update the view
    }
    public override fun onStop() {
        super.onStop()
        onStopCounter++
        //Log cat print out
        Log.i(TAG, "onStop called $onStopCounter time(s)")
        stopView.text="onStop has been called:$onStopCounter time"
        //TODO:  update the appropriate count variable & update the view
    }

    public override fun onRestart() {
        super.onRestart()
        onRestartCounter++
        //Log cat print out
        Log.i(TAG, "onRestart called $onRestartCounter time(s)")
        restartView.text="onRestart has been called:$onRestartCounter time"
        //TODO:  update the appropriate count variable & update the view
    }
    public override fun onDestroy() {
        super.onDestroy()
        onDestroyCounter++
        //Log cat print out
        Log.i(TAG, "onDestroy called $onDestroyCounter time(s)")
        destroyView.text="onDestroy has been called:$onDestroyCounter time"
        //TODO:  update the appropriate count variable & update the view
    }

    // TODO: implement 5 missing lifecycle callback methods with Logging and
    // TODO: increment the counter variables' values when their corresponding lifecycle methods are called and log the info

    // Note:  if you want to use a resource as a string you must do the following
    //  getResources().getString(R.string.stringname)   returns a String.

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        //TODO:  save state information with a collection of key-value pairs & save all  count variables
    }


    fun launchActivityTwo(view: View) {
        startActivity(Intent(this, ActivityTwo::class.java))
    }

    // singleton object in kotlin
    // companion object
    companion object {
        // string for logcat
        private val TAG = "Lab-ActivityOne"
    }


}
